import Component from '@ember/component';
import { inject } from '@ember/service';

export default Component.extend({
    store: inject('store'),
    newBook: null,
    stepNumber: 1,
    actions: {
        changeStep(number, email, phone, isEnrolled ) {
            if (number === 3) {
                const newBook = { email, phone, isEnrolled };
                this.set('newBook', newBook);
                this.store.createRecord('book', newBook).save();
            }
            this.set('stepNumber', number);
        }
    }
});
