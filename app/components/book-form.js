import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
    isEnrolled: false,
    isEmailChecked: false,
    isEmailEntered: computed('email', function() {
        return this.email.length;
    }),
    isWrongEmailEntered: computed('isEmailEntered', 'isEmailChecked', function() {
        return this.isEmailEntered && !this.isEmailChecked;
    }),
    isPhoneChecked: false,
    isPhoneEntered: computed('phone', function() {
        return this.phone.replace(/\D/g, '').length;
    }),
    isWrongPhoneEntered: computed('isPhoneEntered', 'isPhoneChecked', function() {
        return this.isPhoneEntered && !this.isPhoneChecked;
    }),
    isNotDataCorrect: computed('isEmailChecked', 'isPhoneChecked', 'isEnrolled', function () {
      return !this.isEmailChecked || !this.isPhoneChecked;
    }),
    email: "",
    phone: "",
    actions: {
        onChangeRole(value) {
            this.set('isEnrolled', value === "Yes");
        },
        onChangeEmail(e) {
            const email = e.target.value;
            const emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            this.set('email', email);
            this.set('isEmailChecked', email.match(emailRegExp) !== null);
        },
        onChangePhone(e) {
            let phone = e.target.value.replace(/\D/g, '');
            let digitsQuantity = phone.length;
            let currentValue;
            let currentPosition;
            switch (digitsQuantity) {
                case 0: {
                currentValue = '(XXX) XXX-XXXX';
                currentPosition = 1;
                this.set('isPhoneChecked', false);
                break;
                }
                case 1: {
                currentValue = `(${phone[digitsQuantity - 1]}XX) XXX-XXXX`;
                currentPosition = 2;
                this.set('isPhoneChecked', false);
                break;
                }
                case 2: {
                currentValue = `(${phone[digitsQuantity - 2]}${phone[digitsQuantity - 1]}X) XXX-XXXX`;
                currentPosition = 3;
                this.set('isPhoneChecked', false);
                break;
                }
                case 3: {
                currentValue =
                    `(${phone[digitsQuantity - 3]}${phone[digitsQuantity - 2]}${phone[digitsQuantity - 1]}) XXX-XXXX`;
                currentPosition = 4;
                this.set('isPhoneChecked', false);
                break;
                }
                case 4: {
                currentValue =
                    `(${phone[digitsQuantity - 4]}${phone[digitsQuantity - 3]}${phone[digitsQuantity - 2]}) ${phone[digitsQuantity - 1]}XX-XXXX`;
                currentPosition = 7;
                this.set('isPhoneChecked', false);
                break;
                }
                case 5: {
                currentValue =
                    `(${phone[digitsQuantity - 5]}${phone[digitsQuantity - 4]}${phone[digitsQuantity - 3]}) ${phone[digitsQuantity - 2]}${phone[digitsQuantity - 1]}X-XXXX`;
                currentPosition = 8;
                this.set('isPhoneChecked', false);
                break;
                }
                case 6: {
                currentValue =
                    `(${phone[digitsQuantity - 6]}${phone[digitsQuantity - 5]}${phone[digitsQuantity - 4]}) ${phone[digitsQuantity - 3]}${phone[digitsQuantity - 2]}${phone[digitsQuantity - 1]}-XXXX`;
                currentPosition = 9;
                this.set('isPhoneChecked', false);
                break;
                }
                case 7: {
                currentValue =
                    `(${phone[digitsQuantity - 7]}${phone[digitsQuantity - 6]}${phone[digitsQuantity - 5]}) ${phone[digitsQuantity - 4]}${phone[digitsQuantity - 3]}${phone[digitsQuantity - 2]}-${phone[digitsQuantity - 1]}XXX`;
                currentPosition = 11;
                this.set('isPhoneChecked', false);
                break;
                }
                case 8: {
                currentValue =
                    `(${phone[digitsQuantity - 8]}${phone[digitsQuantity - 7]}${phone[digitsQuantity - 6]}) ${phone[digitsQuantity - 5]}${phone[digitsQuantity - 4]}${phone[digitsQuantity - 3]}-${phone[digitsQuantity - 2]}${phone[digitsQuantity - 1]}XX`;
                currentPosition = 12;
                this.set('isPhoneChecked', false);
                break;
                }
                case 9: {
                currentValue =
                    `(${phone[digitsQuantity - 9]}${phone[digitsQuantity - 8]}${phone[digitsQuantity - 7]}) ${phone[digitsQuantity - 6]}${phone[digitsQuantity - 5]}${phone[digitsQuantity - 4]}-${phone[digitsQuantity - 3]}${phone[digitsQuantity - 2]}${phone[digitsQuantity - 1]}X`;
                currentPosition = 13;
                this.set('isPhoneChecked', false);
                break;
                }
                case 10: {
                currentValue =
                    `(${phone[digitsQuantity - 10]}${phone[digitsQuantity - 9]}${phone[digitsQuantity - 8]}) ${phone[digitsQuantity - 7]}${phone[digitsQuantity - 6]}${phone[digitsQuantity - 5]}-${phone[digitsQuantity - 4]}${phone[digitsQuantity - 3]}${phone[digitsQuantity - 2]}${phone[digitsQuantity - 1]}`;
                currentPosition = 14;
                this.set('isPhoneChecked', true);
                break;
                }
                default:
                phone = phone.slice(0, 10);
                digitsQuantity = 10;
                currentValue =
                    `(${phone[digitsQuantity - 10]}${phone[digitsQuantity - 9]}${phone[digitsQuantity - 8]}) ${phone[digitsQuantity - 7]}${phone[digitsQuantity - 6]}${phone[digitsQuantity - 5]}-${phone[digitsQuantity - 4]}${phone[digitsQuantity - 3]}${phone[digitsQuantity - 2]}${phone[digitsQuantity - 1]}`;
                currentPosition = 14;
                this.set('isPhoneChecked', true);
            }
            e.target.value = currentValue;
            e.target.setSelectionRange(currentPosition, currentPosition);
            this.set('phone', currentValue); 
        }
    }
});
