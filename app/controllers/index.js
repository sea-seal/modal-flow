import Controller from '@ember/controller';

export default Controller.extend({
    isShowModal: false,
    actions: {
        toggleModal: function() {
            this.toggleProperty('isShowModal');
        }
    }
});
