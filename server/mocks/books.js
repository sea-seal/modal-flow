/* eslint-env node */
'use strict';

const books = [];

module.exports = function(app) {
  const express = require('express');
  let apiV1BookRouter = express.Router();

  apiV1BookRouter.get('/', function(req, res) {
    res.send({
      'api/v1/books': []
    });
  });

  apiV1BookRouter.post('/', function(req, res) {
    const { email, phone, isEnrolled } = req.body.book;
    const newBook = {
      id: books.length + 1,
      email,
      phone,
      isEnrolled
    };
    books.push(newBook);
    res.send({book: newBook});
  });

  apiV1BookRouter.get('/:id', function(req, res) {
    res.send({
      'api/v1/books': {
        id: req.params.id
      }
    });
  });

  apiV1BookRouter.put('/:id', function(req, res) {
    res.send({
      'api/v1/books': {
        id: req.params.id
      }
    });
  });

  apiV1BookRouter.delete('/:id', function(req, res) {
    res.status(204).end();
  });

  // The POST and PUT call will not contain a request body
  // because the body-parser is not included by default.
  // To use req.body, run:

  //    npm install --save-dev body-parser

  // After installing, you need to `use` the body-parser for
  // this mock uncommenting the following line:
  //
  //app.use('/api/api-v1-book', require('body-parser').json());
  app.use('/api/v1/books', apiV1BookRouter);
};
